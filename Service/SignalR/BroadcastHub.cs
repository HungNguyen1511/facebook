﻿using Dtos.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Service.SignalR
{
    public class BroadcastHub : Hub<IHubClient>
    {
        public async Task SendNotification(NotificationVm notificationVm)
        {
            await Clients.All.ReceiveNotification(notificationVm);
        }
    }
}
