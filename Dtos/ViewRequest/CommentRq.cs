﻿using System;

namespace Dtos.ViewRequest
{
    public class CommentRq
    {
        public string CommentContent { set; get; }
        public int? ParentCommentId { set; get; }
        public Guid? PostId { get; set; }
    }
}
