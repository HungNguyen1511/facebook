﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Domain.Model
{
    public class UserInfo : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string OauthSubject { get; set; }
        public string OauthIssuer { get; set; }
    }
}
