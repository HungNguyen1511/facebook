﻿using static Common.Constant;

namespace Dtos.ViewRequest
{
    public class MediaRq
    {
        public string PostId { get; set; }
        public string FilePath { get; set; }
        public MediaType FileType { get; set; }      
    }
}
