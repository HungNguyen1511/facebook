﻿using Common;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interface.IService;
using System.Threading.Tasks;
using static Common.Constant;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ReactionController : ControllerBase
    {
        private readonly IReactionService _reactionService;
        private readonly INotificationService _notificationService;
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;
        
        public ReactionController(IReactionService reactionService, INotificationService notificationService, IPostService postService, ICommentService commentService)
        {
            _reactionService = reactionService;
            _notificationService = notificationService;
            _postService = postService;
            _commentService = commentService;
        }

        //[HttpGet("post/{postId}")]
        //public async Task<IActionResult> GetReactionPost(Guid postId)
        //{
        //    var listComment = _reactionService.ReactionComment(postId);
        //    return Ok(ApiResponse<object>.Success(listComment));
        //}


        [HttpPost("comment")]
        public async Task<IActionResult> ReactionComment([FromBody] ReactionCommentRq reaction)
        {
            var reactionVm = await _reactionService.ReactionComment(reaction);
            var commentVm = await _commentService.GetComment(reaction.CommentId);
            if(commentVm == null)
                return BadRequest(ApiResponse<object>.Fail("Comment is not found"));
            NotificationRq notificationRq = new NotificationRq()
            {
                UserId = commentVm.UserId,
                NotificationContent = reactionVm,
                Time = reactionVm.ReactionTime,
                Type = NotificationType.Reaction
            };
            await _notificationService.CreateNotification(notificationRq);
            return Ok(ApiResponse<object>.Success(reactionVm));
        }

        [HttpPost("post")]
        public async Task<IActionResult> ReactionPost([FromBody] ReactionPostRq reaction)
        {
            var reactionVm = await _reactionService.ReactionPost(reaction);
            var postVm = await _postService.GetPostById(reaction.PostId);
            if (postVm == null)
                return BadRequest(ApiResponse<object>.Fail("Post is not found"));
            NotificationRq notificationRq = new NotificationRq()
            {
                UserId = postVm.UserId,
                NotificationContent = reactionVm,
                Time = reactionVm.ReactionTime,
                Type = NotificationType.Reaction
            };
            await _notificationService.CreateNotification(notificationRq);
            return Ok(ApiResponse<object>.Success());
        }
    }
}
