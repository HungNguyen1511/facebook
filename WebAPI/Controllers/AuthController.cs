﻿using Common.Helpers;
using Domain.Model;
using Dtos.ViewModel;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private UserManager<UserInfo> userManager;
        private SignInManager<UserInfo> signInManager;
        public AuthController(UserManager<UserInfo> userMgr, SignInManager<UserInfo> signinMgr)
        {
            userManager = userMgr;
            signInManager = signinMgr;
        }

        [AllowAnonymous]
        [HttpPost("google")]
        public async Task<IActionResult> Google([FromBody] UserInfoVm userView)
        {
            try
            {
                var payload = GoogleJsonWebSignature.ValidateAsync(userView.TokenId, new GoogleJsonWebSignature.ValidationSettings()).Result;
                var user = await FindUserOrAdd(payload);
                var userName = payload.Email.Substring(0, payload.Email.LastIndexOf("@"));
                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userName),
                    new Claim(ClaimTypes.Email, payload.Email),
                    new Claim(ClaimTypes.Name, payload.GivenName),
                    new Claim(JwtRegisteredClaimNames.Sub, Security.Encrypt("pyjyrwddxxgzlrnfdrlgknmcorlbiszkvusebfzk",user.Email)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())              
                };

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("pyjyrwddxxgzlrnfdrlgknmcorlbiszkvusebfzk"));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var expires = DateTime.Now.AddDays(1);
                var token = new JwtSecurityToken(String.Empty,
                  String.Empty,
                  claims,
                  expires: expires,
                  signingCredentials: creds);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expires = expires,
                    userName = userName,
                    name = payload.GivenName,
                    avatar = payload.Picture
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }            
        }

        private async Task<UserInfo> FindUserOrAdd(GoogleJsonWebSignature.Payload payload)
        {
            var u = await userManager.FindByEmailAsync(payload.Email);
            if (u == null)
            {
                u = new UserInfo()
                {
                    UserName = payload.Email.Substring(0, payload.Email.LastIndexOf("@")),
                    Email = payload.Email,
                    FirstName = payload.GivenName,
                    OauthSubject = payload.Subject,
                    OauthIssuer = payload.Issuer
                };
                await userManager.CreateAsync(u);
            }
            return u;
        }
    }
}
