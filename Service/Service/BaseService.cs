﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Service.Interface.IUnitOfWork;

namespace Service.Service
{
    public class BaseService
    {
        public IConfiguration Configuration;
        public IMapper Mapper;
        public IUnitOfWork UnitOfWork;
    }
}
