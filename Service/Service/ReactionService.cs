﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Service
{
    public class ReactionService: BaseService, IReactionService
    {
        private readonly IUserHelper UserHelper;
        public ReactionService(IMapper mapper, IUnitOfWork unitOfWork, IUserHelper userHelper)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
            UserHelper = userHelper;
        }

        public async Task<ReactionVm> ReactionPost(ReactionPostRq reactionRq)
        {
            var userId = UserHelper.GetUserId();
            
            var checkReaction = UnitOfWork.ReactionRepository.GetAll().Where(x=>x.PostId == reactionRq.PostId && x.UserId == userId).FirstOrDefault();
            if (checkReaction == null)
            {
                //add reaction to db
                Reaction reaction = Mapper.Map<Reaction>(reactionRq);
                reaction.ReactionTime = DateTime.Now;
                reaction.UserId = userId;
                await UnitOfWork.ReactionRepository.Add(reaction);
                await UnitOfWork.CompleteAsync();
                return Mapper.Map<ReactionVm>(reaction);
            }
            else
            {
                //update reaction if exists
                var newReaction = checkReaction;
                newReaction.Reactions = reactionRq.Reactions;
                newReaction.ReactionTime = DateTime.Now;
                await UnitOfWork.ReactionRepository.Update(newReaction);
                await UnitOfWork.CompleteAsync();
                return Mapper.Map<ReactionVm>(newReaction);
            }
           
        }

        public async Task<ReactionVm> ReactionComment(ReactionCommentRq reactionRq)
        {
            var userId = UserHelper.GetUserId();
            var checkReaction = UnitOfWork.ReactionRepository.GetAll().Where(x => x.CommentId == reactionRq.CommentId && x.UserId == userId).FirstOrDefault();
            if (checkReaction == null)
            {
                Reaction reaction = Mapper.Map<Reaction>(reactionRq);
                reaction.ReactionTime = DateTime.Now;
                reaction.UserId = userId;
                await UnitOfWork.ReactionRepository.Add(reaction);
                await UnitOfWork.CompleteAsync();
                return Mapper.Map<ReactionVm>(reaction);
            }
            else
            {
                var newReaction = checkReaction;
                newReaction.Reactions = reactionRq.Reactions;
                newReaction.ReactionTime = DateTime.Now;
                await UnitOfWork.ReactionRepository.Update(newReaction);
                await UnitOfWork.CompleteAsync();
                return Mapper.Map<ReactionVm>(newReaction);
            }
        }

    }
}
