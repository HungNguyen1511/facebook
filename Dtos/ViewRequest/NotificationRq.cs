﻿using System;
using static Common.Constant;

namespace Dtos.ViewRequest
{
    public class NotificationRq
    {
        public string UserId { get; set; }
        public object NotificationContent { get; set; }
        public DateTime Time { get; set; }
        public NotificationType Type { get; set; }
    }
}
