﻿namespace Common
{
    public static class Constant
    {
        //Api response status 
        public const string ApiResponseFailed = "fail";
        public const string ApiResponseSuccess = "success";
        public enum MediaType
        {
            Image,
            Video,
            Avatar
        }
        public enum ReactionType
        {
            None,
            Like,
            Love,
            Haha,
            Care,
            Sad,
            Wow,
            Angry
        };

        public enum NotificationType
        {
            Post,
            Comment,
            Reaction
        }
    }
}
