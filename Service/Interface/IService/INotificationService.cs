﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface INotificationService
    {
        Task<List<NotificationVm>> GetAll();
        Task<NotificationVm> GetNotificationById(int id);
        Task CreateNotification(NotificationRq notification);
        Task DeleteNotification(int id);
    }
}
