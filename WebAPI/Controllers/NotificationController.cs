﻿using Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interface.IService;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _notificationServices;
        public NotificationController(INotificationService notificationServices)
        {
            _notificationServices = notificationServices;
        }

        [HttpGet]
        public async Task<IActionResult> GetNotification()
        {
            var notifications = await _notificationServices.GetAll();
            return Ok(ApiResponse<object>.Success(notifications));
        }
    }
}
