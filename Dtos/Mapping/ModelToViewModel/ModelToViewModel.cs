﻿using AutoMapper;
using Domain.Model;
using Dtos.ViewModel;
using Newtonsoft.Json;
using static Common.Constant;

namespace Dtos.Mapping.ModelToViewModel
{
    public class ModelToViewModel : Profile
    {
        public ModelToViewModel()
        {
            CreateMap<Comment, CommentVm>();
            CreateMap<Notification, NotificationVm>().ForMember(des => des.NotificationContent, opt => opt.MapFrom((src,des)=>
            {
                object content = src.Type switch
                {
                    NotificationType.Comment => JsonConvert.DeserializeObject<CommentVm>(src.NotificationContent),
                    NotificationType.Reaction => JsonConvert.DeserializeObject<ReactionVm>(src.NotificationContent),
                    NotificationType.Post => JsonConvert.DeserializeObject<PostVm>(src.NotificationContent),
                    _ => throw new AutoMapperMappingException()
                };
                return content;
            }));
            CreateMap<Post, PostVm>();
            CreateMap<PostCategory, PostCategoryVm>();
            CreateMap<Media, MediaVm>();
            CreateMap<Media, PostMediaVm>();
            CreateMap<Reaction, ReactionVm>();
        }
    }
}
