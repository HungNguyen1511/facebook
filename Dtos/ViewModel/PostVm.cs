﻿using System;
using System.Collections.Generic;
using static Common.Constant;

namespace Dtos.ViewModel
{
    public class PostVm
    {
        public string PostId { set; get; }
        public string PostContent { set; get; }
        public DateTime PostTime { set; get; }
        public string UserId { get; set; }
        public int PostCategoryId { get; set; }
        public ReactionType React { get; set; } = ReactionType.None;
        public List<PostMediaVm> Media { get; set; }
    }

    public class PostMediaVm
    {
        public int MediaId { get; set; }
        public string FilePath { get; set; }
        public MediaType FileType { get; set; }
    }
}
