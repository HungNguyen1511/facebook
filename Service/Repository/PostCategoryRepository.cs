﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class PostCategoryRepository : BaseRepository<PostCategory>, IPostCategoryRepository
    {
        private readonly FacebookDBContext _context;

        public PostCategoryRepository(FacebookDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
