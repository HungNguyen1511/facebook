﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface ICommentService
    {
        List<CommentVm> GetCommentByPost(Guid postId);
        Task<CommentVm> GetComment(int commentId);
        Task<CommentVm> CreateComment(CommentRq comment);
        Task EditComment(CommentRq comment);
        Task DeleteComment(int id);
    }
}
