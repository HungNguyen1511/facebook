﻿using Common.Helper;
using Microsoft.Extensions.DependencyInjection;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using Service.Service;
using Service.UnitOfWork;

namespace WebAPI.IOC
{
    public static class NativeInjectorConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IUserHelper, UserHelper>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IMediaService, MediaService>();
            services.AddScoped<IReactionService, ReactionService>();
            services.AddScoped<INotificationService, NotificationService>();
        }
    }
}
