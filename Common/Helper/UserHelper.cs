﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
namespace Common.Helper
{
    public interface IUserHelper
    {
        string GetUserId();
        string GetUserEmail();
        string GetUserName();
    }

    public class UserHelper : IUserHelper
    {
        private readonly IHttpContextAccessor _context;

        public UserHelper(IHttpContextAccessor context)
        {
            _context = context;
        }

        public string GetUserId()
        {
            //return Guid.NewGuid().ToString();
            string userid = _context.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return userid != null ? userid : "c8aac188-dbb8-4609-ae65-ea1af1b779ff";
        }

        public string GetUserEmail()
        {
            string useremail = _context.HttpContext.User?.FindFirst(ClaimTypes.Email)?.Value;
            return useremail != null ? useremail : "testuser@gmail.com";
        }

        public string GetUserName()
        {
            string username = _context.HttpContext.User?.FindFirst(ClaimTypes.Name)?.Value;
            return username != null ? username : "testuser";
        }
    }
}
