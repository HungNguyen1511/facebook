﻿using Newtonsoft.Json;

namespace Dtos.ViewModel
{
    public class UserInfoVm
    {
        [JsonProperty("tokenId")]
        public string TokenId { get; set; }
    }
}
