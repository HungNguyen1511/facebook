﻿using Common;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Interface.IService;
using System;
using System.Threading.Tasks;
using static Common.Constant;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IPostService _postService;
        private readonly INotificationService _notificationService;
        public CommentController(ICommentService commentService, IPostService postService, INotificationService notificationService)
        {
            _commentService = commentService;
            _postService = postService;
            _notificationService = notificationService;
        }

        [HttpGet("{postId}")]
        public async Task<IActionResult> GetComment(Guid postId)
        {
            var listComment = _commentService.GetCommentByPost(postId);
            return Ok(ApiResponse<object>.Success(listComment));
        }


        [HttpPost("create")]
        public async Task<IActionResult> CreateComment([FromBody]CommentRq comment)
        {
            string userId = string.Empty;
            if(comment.PostId != null)
            {
                var user = await _postService.GetPostById(comment.PostId.Value);
                userId = user.UserId;
            }
            else if(comment.ParentCommentId != null)
            {
                var user = await _commentService.GetComment(comment.ParentCommentId.Value);
                userId = user.UserId;
            }
            var result = await _commentService.CreateComment(comment);
            NotificationRq notificationRq = new NotificationRq()
            {
                UserId = userId,
                NotificationContent = result,
                Time = DateTime.Now,
                Type = NotificationType.Comment
            };
            await _notificationService.CreateNotification(notificationRq);
            return Ok(ApiResponse<CommentVm>.Success(result));
        }
    }
}
