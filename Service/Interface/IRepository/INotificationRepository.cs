﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
    }
}
