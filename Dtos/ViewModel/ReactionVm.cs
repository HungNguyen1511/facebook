﻿using Newtonsoft.Json;
using System;
using static Common.Constant;

namespace Dtos.ViewModel
{
    public class ReactionVm
    {
        [JsonProperty("reactionId")]
        public int ReactionId { get; set; }
        [JsonProperty("commentId")]
        public int? CommentId { get; set; }
        [JsonProperty("postId")]
        public Guid? PostId { get; set; }
        [JsonProperty("reactions")]
        public ReactionType Reactions { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("reactionTime")]
        public DateTime ReactionTime { get; set; }
    }
}
