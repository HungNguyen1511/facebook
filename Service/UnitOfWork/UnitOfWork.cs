﻿using Domain.Model;
using Service.Interface.IUnitOfWork;
using Service.Repository;
using System;
using System.Threading.Tasks;

namespace Service.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FacebookDBContext _context;
        private CommentRepository _commentRepository;
        private MediaRepository _mediaRepository;
        private NotificationRepository _notificationRepository;
        private PostCategoryRepository _postCategoryRepository;
        private PostRepository _postRepository;
        private ReactionRepository _reactionRepository;

        public UnitOfWork(FacebookDBContext context)
        {
            _context = context;
        }

        public CommentRepository CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_context);
                }
                return _commentRepository;
            }
        }

        public MediaRepository MediaRepository
        {
            get
            {
                if (_mediaRepository == null)
                {
                    _mediaRepository = new MediaRepository(_context);
                }
                return _mediaRepository;
            }
        }

        public NotificationRepository NotificationRepository
        {
            get
            {
                if (_notificationRepository == null)
                {
                    _notificationRepository = new NotificationRepository(_context);
                }
                return _notificationRepository;
            }
        }

        public PostCategoryRepository PostCategoryRepository
        {
            get
            {
                if (_postCategoryRepository == null)
                {
                    _postCategoryRepository = new PostCategoryRepository(_context);
                }
                return _postCategoryRepository;
            }
        }

        public PostRepository PostRepository
        {
            get
            {
                if (_postRepository == null)
                {
                    _postRepository = new PostRepository(_context);
                }
                return _postRepository;
            }
        }
        
        public ReactionRepository ReactionRepository
        {
            get
            {
                if (_reactionRepository == null)
                {
                    _reactionRepository = new ReactionRepository(_context);
                }
                return _reactionRepository;
            }
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
