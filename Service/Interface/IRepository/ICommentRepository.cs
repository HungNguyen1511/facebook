﻿using Domain.Model;
using System.Linq;

namespace Service.Interface.IRepository
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        IQueryable<Comment> GetAll();
    }
}
