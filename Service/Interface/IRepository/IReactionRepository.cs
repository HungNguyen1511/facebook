﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface IReactionRepository : IBaseRepository<Reaction>
    {
    }
}
