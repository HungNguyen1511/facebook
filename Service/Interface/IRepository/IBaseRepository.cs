﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Interface.IRepository
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {
        Task Add(TEntity obj);
        IQueryable<TEntity> GetAll();
        Task<TEntity> GetById(int id);
        Task<TEntity> GetById(string id);
        Task Update(TEntity obj);
        Task Delete(TEntity obj);
        Task Delete(int id);
    }
}
