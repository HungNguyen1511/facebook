﻿using System;
using static Common.Constant;

namespace Domain.Model
{
    public class Reaction
    {
        public int ReactionId { get; set; }
        public int? CommentId { get; set; }
        public Guid? PostId { get; set; }
        public ReactionType Reactions { get; set; }
        public string UserId { get; set; }
        public DateTime ReactionTime { get; set; }
    }
}
