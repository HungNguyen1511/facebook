﻿using Domain.Model;
using Dtos.Mapping.ModelToRequestModel;
using Dtos.Mapping.ModelToViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using WebAPI.IOC;
using Service.SignalR;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<FacebookDBContext>(options => 
                options.UseSqlServer(
                    Configuration.GetConnectionString("ServerConnection")));
                    //Configuration.GetConnectionString("DefaultConnection")));
            //2. setup identity 
            services.AddIdentity<UserInfo, UserRoleInfo>()
                .AddEntityFrameworkStores<FacebookDBContext>();          
            services.AddAuthentication().AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;

                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("pyjyrwddxxgzlrnfdrlgknmcorlbiszkvusebfzk")),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });        
            //.AddGoogle(opts => {
            //    opts.ClientId = "1088227274470-mc9sa4odhpdbi5n8ggaa91l7drpncfu4.apps.googleusercontent.com";
            //    opts.ClientSecret = "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";
            //});
            services.AddAuthorization();
            services.AddHttpContextAccessor();
            services.AddControllers();
            services.AddCors();
            services.AddAutoMapper(typeof(RequestModelToModel).Assembly);
            services.AddAutoMapper(typeof(ModelToViewModel).Assembly);        
            services.RegisterServices();
            services.AddSignalR();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebAPI", Version = "v1" });              
            });
         
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();             
            }
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseCors(x => x
               .WithOrigins(@"http://localhost:3000", @"https://facebooknetfe.herokuapp.com")
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true)
            .AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();
           
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<BroadcastHub>("/notify");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.OAuthClientId("swagger");
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Facebook API");
            });
        }
    }
}
