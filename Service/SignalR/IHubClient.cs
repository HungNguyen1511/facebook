﻿using Dtos.ViewModel;
using System.Threading.Tasks;

namespace Service.SignalR
{
    public interface IHubClient
    {
        Task ReceiveNotification(NotificationVm notificationVm);
        Task RealTimeComment(CommentVm CommentVm);
    }
}
