﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.SignalR;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using Service.SignalR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static Common.Constant;

namespace Service.Service
{
    public class CommentService : BaseService, ICommentService
    {
        private readonly IUserHelper UserHelper;
        private readonly IHubContext<BroadcastHub, IHubClient> _hubContext;

        public CommentService(IMapper mapper, IUnitOfWork unitOfWork, IHubContext<BroadcastHub, IHubClient> hubContext, IUserHelper userHelper)
        {
            Mapper = mapper;
            UserHelper = userHelper;
            UnitOfWork = unitOfWork;
            _hubContext = hubContext;
        }
        public async Task<CommentVm> CreateComment(CommentRq commentrq)
        {
            Comment comment = Mapper.Map<Comment>(commentrq);
            comment.CommentTime = DateTime.Now;
            comment.UserId = UserHelper.GetUserId();
            await UnitOfWork.CommentRepository.Add(comment);
            await UnitOfWork.CompleteAsync();
            CommentVm commentVm = Mapper.Map<CommentVm>(comment);
            await _hubContext.Clients.All.RealTimeComment(commentVm);
            return commentVm;
        }

        public async Task DeleteComment(int id)
        {
            await UnitOfWork.PostRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditComment(CommentRq commentrq)
        {
            Comment comment = Mapper.Map<Comment>(commentrq);
            await UnitOfWork.CommentRepository.Update(comment);
            await UnitOfWork.CompleteAsync();
        }


        public List<CommentVm> GetCommentByPost(Guid postId)
        {
            var listComment = UnitOfWork.CommentRepository.GetAll().Where(x => x.ParentCommentId == null && x.PostId.Value == postId).OrderBy(p=>p.CommentTime).ToList();
            var listCommentVm = new List<CommentVm>();
            foreach (var comment in listComment)
            {
                var commentVm = Mapper.Map<CommentVm>(comment);
                commentVm.React = GetReactionComment(comment.CommentId);
                commentVm.CommentChild = GetChildComment(comment.CommentId);
                listCommentVm.Add(commentVm);
            }

            return listCommentVm;
        }

        public async Task<CommentVm> GetComment(int commentId)
        {
            var comment = await UnitOfWork.CommentRepository.GetById(commentId);
            return Mapper.Map<CommentVm>(comment);
        }

        private List<CommentVm> GetChildComment(int commentId)
        {
            var listChild = UnitOfWork.CommentRepository.GetAll().Where(p => p.ParentCommentId == commentId).OrderBy(x => x.CommentTime).ToList();
            var listChildVm = listChild.Select(p =>
            {
                var commentVm = Mapper.Map<CommentVm>(p);
                commentVm.CommentChild = GetChildComment(p.CommentId);
                commentVm.React = GetReactionComment(p.CommentId);
                return commentVm;
            }).ToList();
            if (listChildVm != null)
                return listChildVm;
            else
                return new List<CommentVm>();
        }

        private ReactionType GetReactionComment(int commentId)
        {
            var userId = UserHelper.GetUserId();
            var result = UnitOfWork.ReactionRepository.GetAll().Where(x => x.CommentId == commentId && x.UserId == userId).FirstOrDefault();
            if (result == null)
                return ReactionType.None;
            return result.Reactions;
        }
    }
}
