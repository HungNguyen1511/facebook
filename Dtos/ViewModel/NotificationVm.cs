﻿using System;
using static Common.Constant;

namespace Dtos.ViewModel
{
    public class NotificationVm
    {
        public int NotificationId { get; set; }
        public string UserId { get; set; }
        public object NotificationContent { get; set; }
        public DateTime Time { get; set; }
        public NotificationType Type { get; set; }
    }
}
