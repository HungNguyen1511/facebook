﻿using Service.Repository;
using System;
using System.Threading.Tasks;

namespace Service.Interface.IUnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        CommentRepository CommentRepository { get; }
        MediaRepository MediaRepository { get; }
        NotificationRepository NotificationRepository { get; }
        PostCategoryRepository PostCategoryRepository { get; }
        PostRepository PostRepository { get; }
        ReactionRepository ReactionRepository { get; }
        Task CompleteAsync();
    }
}
