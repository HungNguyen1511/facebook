﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Common.Constant;

namespace Dtos.ViewModel
{
    public class CommentVm
    {
        [JsonProperty("commentId")]
        public int CommentId { set; get; }
        [JsonProperty("commentContent")]
        public string CommentContent { set; get; }
        [JsonProperty("parentCommentId")]
        public int? ParentCommentId { set; get; }
        [JsonProperty("commentChild")]
        public List<CommentVm> CommentChild { set; get; }
        [JsonProperty("commentTime")]
        public DateTime CommentTime { set; get; }
        [JsonProperty("postId")]
        public Guid? PostId { get; set; }
        [JsonProperty("react")]
        public ReactionType React { set; get; } = ReactionType.None;
        [JsonProperty("userId")]
        public string UserId { get; set; }
    }
}
