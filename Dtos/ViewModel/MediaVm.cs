﻿using static Common.Constant;

namespace Dtos.ViewModel
{
    public class MediaVm
    {
        public int MediaId { get; set; }
        public string PostId { get; set; }
        public string FilePath { get; set; }
        public MediaType FileType { get; set; }
    }
}
