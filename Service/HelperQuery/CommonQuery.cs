﻿namespace Service.HelperQuery
{
    public static class CommonQuery
    {
       public static string GetAll(string table)
       {
            return $"SELECT * FROM {table}";
       }
    }
}
