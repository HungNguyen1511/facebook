﻿using AutoMapper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.EntityFrameworkCore;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Service
{
    public class MediaService : BaseService, IMediaService
    {
        public MediaService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
        }

        public async Task<List<MediaVm>> GetAll()
        {
            return await Mapper.ProjectTo<MediaVm>(UnitOfWork.MediaRepository.GetAll()).ToListAsync();
        }

        public async Task<MediaVm> GetMediaById(int id)
        {
            Media media = await UnitOfWork.MediaRepository.GetById(id);
            return Mapper.Map<MediaVm>(media);
        }


        public async Task CreateMedia(MediaRq mediaRq)
        {
            Media media = Mapper.Map<Media>(mediaRq);
            //media.UserId = UserHelper.GetUserId().ToString();
            await UnitOfWork.MediaRepository.Add(media);
            await UnitOfWork.CompleteAsync();
        }

        public async Task DeleteMedia(int id)
        {
            await UnitOfWork.PostRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditMedia(MediaRq mediaRq)
        {
            Media media = Mapper.Map<Media>(mediaRq);
            await UnitOfWork.MediaRepository.Update(media);
            await UnitOfWork.CompleteAsync();
        }

        public Task<List<MediaVm>> GetByUser(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<MediaVm> GetMediaByPostId(int postId)
        {
            throw new NotImplementedException();
        }
    }
}
