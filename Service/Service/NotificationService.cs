﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using Service.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Service
{
    public class NotificationService : BaseService, INotificationService
    {
        private readonly IHubContext<BroadcastHub, IHubClient> _hubContext;
        private readonly IUserHelper UserHelper;
        public NotificationService(IMapper mapper, IUnitOfWork unitOfWork, IHubContext<BroadcastHub, IHubClient> hubContext, IUserHelper userHelper)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
            _hubContext = hubContext;
            UserHelper = userHelper;
        }

        public async Task<List<NotificationVm>> GetAll()
        {
            var notifications = UnitOfWork.NotificationRepository.GetAll().Where(n=>n.UserId == UserHelper.GetUserId()).OrderByDescending(c=>c.Time);
            var notificationVms = new List<NotificationVm>();
            foreach (var notification in notifications)
            {
                var notificationVm = Mapper.Map<NotificationVm>(notification);
                notificationVms.Add(notificationVm);
            }
            return notificationVms;
        }

        public async Task<NotificationVm> GetNotificationById(int id)
        {
            Notification notification = await UnitOfWork.NotificationRepository.GetById(id);
            return Mapper.Map<NotificationVm>(notification);
        }


        public async Task CreateNotification(NotificationRq notificationRq)
        {
            notificationRq.NotificationContent = JsonConvert.SerializeObject(notificationRq.NotificationContent);
            Notification notification = Mapper.Map<Notification>(notificationRq);
            await UnitOfWork.NotificationRepository.Add(notification);
            await UnitOfWork.CompleteAsync();
            var notificationVm = Mapper.Map<NotificationVm>(notification);
            await _hubContext.Clients.All.ReceiveNotification(notificationVm);
        }

        public async Task DeleteNotification(int id)
        {
            await UnitOfWork.NotificationRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditMedia(NotificationRq notificationRq)
        {
            Notification notification = Mapper.Map<Notification>(notificationRq);
            await UnitOfWork.NotificationRepository.Update(notification);
            await UnitOfWork.CompleteAsync();
        }

        public Task<List<MediaVm>> GetByUser(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<MediaVm> GetMediaByPostId(int postId)
        {
            throw new NotImplementedException();
        }
    }
}
