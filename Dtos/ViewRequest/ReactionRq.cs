﻿using System;
using static Common.Constant;

namespace Dtos.ViewRequest
{
    public class ReactionRq
    {
        public ReactionType Reactions { get; set; }
    }
    public class ReactionCommentRq:ReactionRq
    {
        public int CommentId { get; set; }
    }
    public class ReactionPostRq:ReactionRq
    {
        public Guid PostId { get; set; }
    }

}
