﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface IReactionService
    {
        Task<ReactionVm> ReactionPost(ReactionPostRq reactionRq);

        Task<ReactionVm> ReactionComment(ReactionCommentRq reactionRq);
    }
}
