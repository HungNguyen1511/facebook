﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.EntityFrameworkCore;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Common.Constant;

namespace Service.Service
{
    public class PostService : BaseService, IPostService
    {
        private readonly IUserHelper UserHelper;

        public PostService(IMapper mapper, IUnitOfWork unitOfWork, IUserHelper userHelper)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
            UserHelper = userHelper;
        }

        public async Task<List<PostVm>> GetAll()
        {
            var listPost = UnitOfWork.PostRepository.GetAll().OrderByDescending(x => x.PostTime).ToList();
            var listPostVm = new List<PostVm>();
            listPost.ForEach(post =>
            {
                PostVm postVm = Mapper.Map<PostVm>(post);
                var listMedia = UnitOfWork.MediaRepository.GetAll().Where(m => m.PostId == postVm.PostId);
                postVm.Media = Mapper.ProjectTo<PostMediaVm>(listMedia).ToList();
                postVm.React = GetReactionPost(postVm.PostId);
                listPostVm.Add(postVm);
            });
            return listPostVm;
        }

        public async Task<PostVm> GetPostById(Guid id)
        {
            Post post = await UnitOfWork.PostRepository.GetAll().FirstOrDefaultAsync(x=>x.PostId == id);
            PostVm postVm = Mapper.Map<PostVm>(post);
            var listMedia = UnitOfWork.MediaRepository.GetAll().Where(m => m.PostId == id.ToString());
            postVm.Media = Mapper.ProjectTo<PostMediaVm>(listMedia).ToList();
            postVm.React = GetReactionPost(postVm.PostId);
            return postVm;
        }


        public async Task<PostVm> CreatePost(Guid postId, PostRq postRq)
        {
            Post post = Mapper.Map<Post>(postRq);
            post.PostId = postId;
            post.UserId = UserHelper.GetUserId();
            post.PostTime = DateTime.Now;
            await UnitOfWork.PostRepository.Add(post);
            await UnitOfWork.CompleteAsync();
            return Mapper.Map<PostVm>(post);
        }

        public async Task DeletePost(int id)
        {
            await UnitOfWork.PostRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditPost(PostRq postrq)
        {
            Post post = Mapper.Map<Post>(postrq);
            await UnitOfWork.PostRepository.Update(post);
            await UnitOfWork.CompleteAsync();
        }

        public Task<List<PostVm>> GetByUser(string id)
        {
            throw new NotImplementedException();
        }

        private ReactionType GetReactionPost(string postId)
        {
            var userId = UserHelper.GetUserId();
            var result = UnitOfWork.ReactionRepository.GetAll().Where(x => x.PostId == Guid.Parse(postId) && x.UserId == userId).FirstOrDefault();
            if (result == null)
                return ReactionType.None;
            return result.Reactions;
        }
    }
}
