﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Dtos.ViewRequest
{
    public class PostRq
    {
        public string PostContent { set; get; }     
        public int PostCategoryId { get; set; }
        public List<IFormFile> PostMedia { get; set; }
    }
}
